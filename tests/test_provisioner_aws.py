"""Test cases for aws module."""
import itertools
import os
import unittest
from unittest import mock

from botocore.exceptions import ClientError

from tests import const
from upt.const import EC2_INSTANCE_RUNNING
from upt.plumbing.format import ProvisionData


class TestAWS(unittest.TestCase):
    """Testcase for AWS provisioner."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(const.ASSETS_DIR, 'aws_req.yaml')
        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.aws = self.provision_data.get_provisioner('aws')

    def test_update_provisioning_request(self):
        """Ensure update_provisioning_request works and sets hostname."""
        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'instance') as mock_inst:
            mock_inst.public_dns_name = 'hostname1'

            self.aws.update_provisioning_request(self.aws.rgs[0])
            self.assertEqual(str(list(self.aws.hosts())[0].hostname), 'hostname1')

    def test_release_resources(self):
        """Ensure release_resources works with instance."""
        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'instance') as mock_inst:
            self.aws.release_resources()
            mock_inst.terminate.assert_called()

    def test_release_resources_no_instance(self):
        """Ensure release_resources works with instance."""
        with mock.patch.object(self.aws.ec2_resource, 'Instance') as mock_inst:
            mock_inst.return_value.terminate.return_value = None

            self.aws.release_resources()
            mock_inst.return_value.terminate.assert_called()

    def test_provision_disabled(self):
        """Ensure provision works."""
        aws = ProvisionData.deserialize_file(self.req_asset).get_provisioner('aws')
        aws.rgs[0].recipeset.hosts[0].misc = {'instance_id': None}
        with mock.patch.object(aws.ec2_resource, 'Instance') as mock_inst:
            with mock.patch.object(aws, 'provision_host', lambda *args: mock_inst):
                aws.provision(**{})

    def test_is_provisioned(self):
        """Ensure is_provisioned calls get_provisioning_state that returns 2 item tuple."""
        with mock.patch.object(self.aws, 'get_provisioning_state', lambda *args: (True, None)):
            self.assertTrue(self.aws.is_provisioned([None]))

    @mock.patch.dict(os.environ, {
        'AWS_UPT_LAUNCH_TEMPLATE_NAME': 'template',
        'AWS_UPT_INSTANCE_PREFIX': const.AWS_UPT_INSTANCE_PREFIX,
        'CI_PIPELINE_ID': '123',
        'CI_JOB_ID': '456',
    })
    def test_provision_host(self):
        """Ensure provision_host works / is called with correct params."""
        with mock.patch.object(self.aws.ec2_client, 'create_fleet') as mock_create_fleet:
            host = list(self.aws.hosts())[0]
            self.aws.provision_host(host)
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [],
                }],
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{host.recipe_id}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': '456'}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    @mock.patch.dict(os.environ, {
        'AWS_UPT_LAUNCH_TEMPLATE_NAME': 'template',
        'AWS_UPT_INSTANCE_PREFIX': const.AWS_UPT_INSTANCE_PREFIX,
        'CI_PIPELINE_ID': '123',
        'CI_JOB_ID': '456',
        'AWS_UPT_IMAGE_ID': 'image',
    })
    def test_provision_host_kwarg_override(self):
        """Ensure provision_host correctly overrides kwargs if requested."""
        with mock.patch.object(self.aws.ec2_client, 'create_fleet') as mock_create_fleet:
            host = list(self.aws.hosts())[0]
            self.aws.provision_host(host)
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [{'ImageId': 'image'}],
                }],
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{host.recipe_id}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': '456'}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    @mock.patch.dict(os.environ, {
        'AWS_UPT_LAUNCH_TEMPLATE_NAME': 'template',
        'AWS_UPT_INSTANCE_PREFIX': const.AWS_UPT_INSTANCE_PREFIX,
        'CI_PIPELINE_ID': '123',
        'CI_JOB_ID': '456',
        'AWS_UPT_IMAGE_ID': '',
    })
    def test_provision_host_kwarg_override_empty(self):
        """Ensure provision_host correctly ignores empty overrides."""
        with mock.patch.object(self.aws.ec2_client, 'create_fleet') as mock_create_fleet:
            host = list(self.aws.hosts())[0]
            self.aws.provision_host(host)
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [],
                }],
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{host.recipe_id}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': '456'}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    @mock.patch.dict(os.environ, {
        'AWS_UPT_LAUNCH_TEMPLATE_NAME': 'template',
        'AWS_UPT_INSTANCE_PREFIX': const.AWS_UPT_INSTANCE_PREFIX,
        'CI_PIPELINE_ID': '123',
        'CI_JOB_ID': '456',
        'AWS_UPT_NETWORK_SUBNET_IDS': 'subnet-1 subnet-2',
        'AWS_UPT_IMAGE_ID': 'image',
    })
    def test_provision_host_kwarg_override_subnet_ids(self) -> None:
        """Ensure provision_host correctly overrides subnet ids if requested."""
        with mock.patch.object(self.aws.ec2_client, 'create_fleet') as mock_create_fleet:
            host = list(self.aws.hosts())[0]
            self.aws.provision_host(host)
            params = {
                'LaunchTemplateConfigs': [{
                    'LaunchTemplateSpecification': {
                        'LaunchTemplateName': 'template',
                        'Version': '$Default',
                    },
                    'Overrides': [
                        {'ImageId': 'image', 'SubnetId': 'subnet-1'},
                        {'ImageId': 'image', 'SubnetId': 'subnet-2'},
                    ],
                }],
                'TagSpecifications': [
                    {'ResourceType': 'instance',
                     'Tags': [
                         {'Key': 'Name',
                          'Value': f'{const.AWS_UPT_INSTANCE_PREFIX}.{host.recipe_id}'},
                         {'Key': 'CkiGitLabPipelineId',
                          'Value': '123'},
                         {'Key': 'CkiGitLabJobId',
                          'Value': '456'}
                     ]}
                ],
                'TargetCapacitySpecification': {
                    'DefaultTargetCapacityType': 'on-demand',
                    'TotalTargetCapacity': 1,
                },
                'Type': 'instant',
            }
            mock_create_fleet.assert_called_with(**params)

    def test_reprovision_aborted(self):
        """Ensure reprovision_aborted works."""
        host2reprovision = list(self.aws.hosts())[0]
        with mock.patch.object(host2reprovision, 'instance') as mock_inst:
            with mock.patch.object(self.aws, 'provision_host') as mock_provision_host:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING + 1}
                self.aws.reprovision_aborted(self.aws.rgs[0])

                mock_provision_host.assert_called()
                self.assertEqual(len(self.aws.rgs[0].erred_rset_ids), 1)

    def test_no_reprovision_aborted(self):
        """Ensure reprovision_aborted isn't called when all hosts are in OK state."""
        host2reprovision = list(self.aws.hosts())[0]
        with mock.patch.object(host2reprovision, 'instance') as mock_inst:
            with mock.patch.object(self.aws, 'provision_host') as mock_provision_host:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
                self.aws.reprovision_aborted(self.aws.rgs[0])

                mock_provision_host.assert_not_called()
                self.assertEqual(len(self.aws.rgs[0].erred_rset_ids), 0)

    @mock.patch('subprocess.run')
    def test_get_provisioning_state(self, mock_run):
        """Ensure get_provisioning_state returns provisioned when instances are running."""
        rg2test = self.aws.rgs[0]
        mock_run.return_value.returncode = 0

        with mock.patch.object(self.aws.ec2_resource, 'Instance') as mock_inst:
            mock_inst.return_value.state = {'Code': EC2_INSTANCE_RUNNING}

            provisioned, erred = self.aws.get_provisioning_state(rg2test)
            self.assertEqual(erred, [])
            self.assertTrue(provisioned)

    @mock.patch('subprocess.run')
    def test_heartbeat(self, mock_run):
        """Ensure heartbeat works."""
        rg2test = self.aws.rgs[0]
        mock_run.return_value.returncode = 0

        with mock.patch.object(self.aws.ec2_resource, 'Instance') as mock_inst:
            mock_inst.return_value.state = {'Code': EC2_INSTANCE_RUNNING + 1}

            recipe_ids_dead = set()
            self.aws.heartbeat(rg2test, recipe_ids_dead)

            self.assertEqual(recipe_ids_dead, {1})

    @mock.patch('upt.logger.LOGGER.debug')
    def test_get_provisioning_state_no_instance(self, mock_debug):
        """Ensure get_provisioning_state returns not provisioned for ClientError."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, 'instance') as mock_inst:
            mock_inst.id = 1
            client_error = ClientError(
                error_response={'Error': {'Code': 'InvalidInstanceID.NotFound'}},
                operation_name='DescribeInstances')
            mock_inst.reload.side_effect = itertools.chain([client_error], itertools.cycle([None]))

            provisioned, erred = self.aws.get_provisioning_state(self.aws.rgs[0])
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)
            mock_debug.assert_called_with('Waiting for host %s to be created', mock_inst.id)

    def test_get_provisioning_state_no_dns(self):
        """Ensure get_provisioning_state returns not provisioned without public dns name."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, 'instance') as mock_inst:
            mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
            mock_inst.public_dns_name = ''

            provisioned, erred = self.aws.get_provisioning_state(self.aws.rgs[0])
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)
            mock_inst.reload.assert_called()

    @mock.patch('subprocess.run')
    def test_get_provisioning_state_no_user_data(self, mock_run):
        """Ensure get_provisioning_state doesn't call ssh readiness after user script."""

        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'reachable_via_ssh', True):
            with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'instance') as mock_inst:
                mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}
                self.aws.get_provisioning_state(self.aws.rgs[0])
                mock_run.assert_not_called()

    @mock.patch('subprocess.run')
    @mock.patch('upt.logger.LOGGER.debug')
    def test_get_provisioning_state_ssh(self, mock_debug, mock_run):
        """Ensure get_provisioning_state uses ssh readiness check when user script isn't done."""
        rg2test = self.aws.rgs[0]
        mock_run.return_value.stdout = 'some output'
        mock_run.return_value.stderr = 'some error'
        mock_run.return_value.returncode = 1

        with mock.patch.object(self.aws.rgs[0].recipeset.hosts[0], 'instance') as mock_inst:
            mock_inst.id = 1
            mock_inst.state = {'Code': EC2_INSTANCE_RUNNING}

            provisioned, erred = self.aws.get_provisioning_state(rg2test)
            self.assertEqual(erred, [])
            self.assertFalse(provisioned)

            mock_debug.assert_any_call(mock.ANY, 'some output')
            mock_debug.assert_any_call(mock.ANY, 'some error')
            mock_debug.assert_any_call(
                'Waiting for host %s to finish UserData script', mock_inst.id)

    def test_get_resource_ids(self):
        """Ensure test_get_resource_ids returns instance ids."""
        host2process = self.aws.rgs[0].recipeset.hosts[0]
        with mock.patch.object(host2process, 'misc', {'instance_id': 'i-1234'}):
            self.assertEqual(['i-1234'], self.aws.get_resource_ids())
