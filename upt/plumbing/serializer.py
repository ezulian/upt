"""UPT provisioning request (de)serialization."""
from upt.logger import LOGGER


class DeserializerBase:
    """Yaml deserializer helper."""

    @classmethod
    def to_yaml(cls, representer, data):
        """Serialize yaml as a given class.

        The class to be serialized needs to have a field `fields_to_serialize`,
        this field needs to be a list containing the names of the fields
        to be serialized.
        """
        data2serialize = {key: value for key, value in
                          data.items() if key in data.fields_to_serialize}

        return representer.represent_mapping(cls.__name__, data2serialize)

    def update(self, new_data):
        """Update the internal dict with new data."""
        for key, value in new_data.items():
            setattr(self, key, value)

    def items(self):
        # pylint: disable=no-member
        """Return dict-like data."""
        return [(key, getattr(self, key, None)) for key in self.__annotations__]

    def to_mapping(self):
        """Convert to kci-db compatible mapping."""
        mapping = {}
        for key, value in self.items():
            try:
                mapping[key] = value.to_mapping()
            except AttributeError:
                if value is not None:
                    mapping[key] = value

        return mapping

    def dicts_to_classes(self, dict_data):
        # pylint: disable=no-member
        """Convert attributes into classes according to their annotations."""
        for key, actual_type in self.__annotations__.items():
            # create empty values for non-required attributes
            val = getattr(self, key, None)
            try:
                if key not in dict_data:
                    if actual_type in (int, float, bool, str, list) or 'enum' in str(actual_type):
                        dict_data[key] = val
                    else:
                        dict_data[key] = {}
            except TypeError:
                continue

        for key, value in filter(lambda kv: kv[0] in self.__annotations__, dict_data.items()):
            actual_type = self.__annotations__[key]
            if 'List[' in str(actual_type):
                cltype = actual_type.__args__[0]
                nested_values = [cltype(item) for item in value]
                setattr(self, key, nested_values)
                continue
            if actual_type is dict:
                setattr(self, key, value)
                continue

            # handle NoneTypes like x: int = None
            if value is None:
                setattr(self, key, value)
                continue

            try:
                setattr(self, key, actual_type(value))
            except TypeError:
                LOGGER.error('conversion failed for %s %s %s', key, value, actual_type)
                raise

    def __init__(self, dict_data=None):
        """Create an object."""
        self.safe_init({} if dict_data is None else dict_data)
        self.dicts_to_classes({} if dict_data is None else dict_data)

    def safe_init(self, data_dict):
        # pylint: disable=no-member
        """Create an object."""
        for key, value in data_dict.items():
            if key not in self.__annotations__:
                # create annotations for undefined keys
                self.__annotations__[key] = type(value)

            setattr(self, key, value)
