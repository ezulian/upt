"""Restraint Job Element."""

from __future__ import annotations

from collections import Counter
import copy
import itertools
import typing
import xml.etree.ElementTree as ET

from .common_element import CommonXMLElement
from .recipe_element import RestraintRecipe
from .recipeset_element import RestraintRecipeSet
from .restraint_dataclasses import RestraintJobDiff
from .task_element import RestraintTask


class RestraintJob(CommonXMLElement):
    """Job class.

    This class contains a list of recipeSets.
    You can see an example in examples.py file (job_example).
    """

    _attributes = ['group']
    _name = 'job'
    _delegated_items = ['recipeSet']

    def __init__(self, element: ET.Element):
        """Initialize."""
        super().__init__(element)
        self.recipesets = self._process_recipesets()
        self._clear()

    @property
    def is_cki_id_used_globally(self) -> bool:
        """
        Return if all tasks are using CKI_ID.

        2023-02-20
        We're introducing a new way to identify task in restraint.
        We want to introduce CKI_ID as a valid identifier for a task.

        https://gitlab.com/cki-project/upt/-/issues/103

        This method should be temporary to maintain backward compatibility.
        """
        for recipe in self.get_all_recipes():
            for task in recipe.tasks:
                if not task.cki_id:
                    return False
        return True

    @property
    def status(self) -> str:
        """
        Get job status based in all recipes.

        Job does not have the status keyword in the Restraint XML file
        but we want to have an easy way to know the job status.

        We want to control this status in this object instead of generating
        external looping.

        Looking at the restraint documentation a recipe can have the following statuses:

        * New
        * Running
        * Completed
        * Aborted

        https://beaker-project.org/docs/alternative-harnesses/

        Rules to define the status:

        * New -> if all recipes have the status New or without status.
        * Running -> if any recipe have a Running status
        * Completed -> if all recipes have Completed status.
        * Aborted -> if at least one recipe has the Aborted status and other have Completed.
                     If previous conditions don't match, we can assume it.
        """
        status_counter = Counter(
            str(recipe.status) for recipe in self.get_all_recipes()
        )

        total = sum(status_counter.values())

        if status_counter["None"] + status_counter["New"] == total:
            return 'New'

        if status_counter["Running"] > 0:
            return 'Running'

        if status_counter["Completed"] == total:
            return 'Completed'

        return 'Aborted'

    def _process_recipesets(self) -> typing.List[RestraintRecipeSet]:
        """Process recipesets."""
        return [RestraintRecipeSet(recipeset) for recipeset in self.element.findall('.//recipeSet')]

    def generate_xml_object(self) -> ET.Element:
        """Generate the object."""
        # clean the element
        self._clear()
        # Generate and add recipesets
        for recipeset in self.recipesets:
            self.element.append(recipeset.generate_xml_object())
        return copy.copy(self.element)

    def get_recipe_by_id(self, recipe_id: int) -> typing.Optional[RestraintRecipe]:
        """Get RestraintRecipe by id.

        Args:
            recipe_id - int, the id that uniquely identifies the RestraintRecipe.
        """
        for recipeset in self.recipesets:
            if recipe := recipeset.get_recipe_by_id(recipe_id):
                return recipe
        return None

    def get_all_recipes(self) -> typing.List[RestraintRecipe]:
        """Get all RestraintRecipe associated to this object.

        Returns:
           list - A list with the recipes for all its RestraintRecipeSet.
        """
        return list(itertools.chain(*[recipeset.recipes for recipeset in self.recipesets]))

    def get_task_by_id(self, recipe_id: int, task_id: int) -> typing.Optional[RestraintTask]:
        """Get a task given the recipe_id and task_id, return None if not found.

        Recipe ids are unique but all tasks in a recipe always start at 1
        """
        if recipe := self.get_recipe_by_id(recipe_id):
            return recipe.get_task_by_id(task_id)
        return None

    def get_task_by_cki_id(self, cki_id: int) -> typing.Optional[RestraintTask]:
        """Get a task given the cki_id, return None if not found.

        CKI_ID is unique
        """
        for recipe in self.get_all_recipes():
            if (task := recipe.get_task_by_cki_id(cki_id)):
                return task
        return None

    def diff(self, other: RestraintJob) -> typing.Optional[RestraintJobDiff]:
        """Generate a diff between two jobs, including recipeset diff.

        A RestraintJob never should change the number of recipesets,
        that's why we don't compare the number of recipesets and its id,
        in the same Job.

        RecipeJob only has group attribute, so we can't compare its attribute, we only
        can compare recipeset (delegated item).

        RestraintJob usually does not have group field in job.xml files, so we don't check
        the group field.
        """
        if not isinstance(other, RestraintJob):
            raise TypeError('Both objects must be RestraintJob')

        recipesets_changes = [
            recipeset_changes
            for recipeset, other_recipeset in zip(self.recipesets, other.recipesets)
            if (recipeset_changes := recipeset.diff(other_recipeset))
        ]

        if recipesets_changes:
            return RestraintJobDiff(self.group, [], recipesets_changes)

        return None
