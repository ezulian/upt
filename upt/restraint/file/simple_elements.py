"""Simple Elements."""

from .common_element import CommonXMLElement


class RestraintFetch(CommonXMLElement):
    """Fetch class.

    You can see an example in examples.py file (fetch_example).
    """

    _attributes = ['url']
    _name = 'fetch'


class RestraintLog(CommonXMLElement):
    """Logs class.

    You can see an example in examples.py file (log_example).
    """

    _attributes = ['filename', 'path']
    _name = 'log'


class RestraintParam(CommonXMLElement):
    """Param class.

    You can see an example in examples.py file (param_example).
    """

    _attributes = ['name', 'value']
    _name = 'param'
